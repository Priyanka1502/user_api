from django.shortcuts import render
from .serializer import RegisterSerializer,LoginSerilizer,PriyaUserSerializer
from .models import priyauser
from rest_framework.permissions import AllowAny
from rest_framework import status
from rest_framework.decorators import api_view,permission_classes
import requests

from django.contrib.auth import authenticate
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings

# Create your views here.

## function Based View

@api_view(['POST'])
@permission_classes([AllowAny])
def Register(request):
    print(request.data)
    print("after views")
    serializer=RegisterSerializer(data=request.data)
    if serializer.is_valid():
        user=serializer.save()
        if user:
            return Response(serializer.data,status=status.HTTP_201_CREATED)
    return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([AllowAny])
def Login(request):
    serializer=Loginserializer(data=request.data)
    if serializer.is_valid():
        user=priyauser.objects.get(email=serializer.data["email"])
        user_auth=authenticate(email=user.email,password=serializer.data["password"])
        if user_auth:
            jwt_payload_handler=api_settings.jwt_payload_handler
            jwt_encode_handler+api_settings.jwt_encode_handler
            payload=jwt_payload_handler(user)
            token=jwt_encode_handler(payload)
            return Response(token,status=status.HTTP_200_OK)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

#password change curd operation

@api_view(['POST'])
@permission_classes([AllowAny])
def GetAllUsers(request):
    userdata=priyauser.objects.all()
    serializer=PriyaUserSerializer(userdata,many=True)
    if serializer.is_valid:
        return Response(serializer.data,status=status.HTTP_200_OK)

@api_view(['POST'])
@permission_classes([AllowAny])
def UpdateUser(request,pk):
    dt=priyauser.objects.get(id=pk)
    serializer=PriyaUserSerializer(instance=dt,date=request.date)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data,status=status.HTTP_200_OK)
    return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
@permission_classes([AllowAny])
def DeleteUser(request,pk):
    user=priyauser.objects.get(id=pk)
    user.delete()
    Response("Deleted Successfully",status=status.HTTP_200_OK)

@api_view(['GET'])
@permission_classes([AllowAny])
def GetSingleUser(request,pk):
    user=priyauser.objects.get(id=pk)
    serializer=PriyaUserSerializer(instance=dt,many=False)
    return Response(serializer.data,status=status.HTTP_200_OK)

   


# Create your views here.
