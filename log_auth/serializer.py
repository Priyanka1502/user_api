from rest_framework import serializers
from .models import priyauser
from rest_framework.validators import UniqueValidator
from django.contrib.auth.hashers import check_password

class RegisterSerializer(serializers.Serializer):
    email=serializers.EmailField(required=True,validators=[UniqueValidator(queryset=priyauser.objects.all())])
    password=serializers.CharField(required=True)
    name=serializers.CharField(required=True)
    phone=serializers.CharField(required=True)

    def create(self,validated_data):
        print(validated_data)
        print(validated_data["name"])
        print(validated_data["phone"])
        try:
            user=priyauser.objects.get(email=validated_data["email"])
            raise ValidationError({"response":"User is Already Existed Please Login"})
        except priyauser.DoesNotExist:
            user=priyauser.objects.create_user(**validated_data)
            return user

class LoginSerilizer(serializers.Serializer):
    email=serializers.EmailField(required=True)
    password=serializers.CharField(required=True)

    def validate(self,validated_data):
        user=priyauser.objects.filter(email=validated_data["email"])
        if not user.exists():
            raise Serializers.ValidationError({"email":"email not found"})

        if not check_password(validated_data["password"],user.first().password):
            raise Serializers.ValidationError({"password":"incorect password"})

        return validated_data


    def passwordchange(request):
        serializer=passwordchangeserializer(request.data)
        if serializer.is_valid():
            user=priyauser.objects.get(email=serializer.data["email"])
            user.set_password(serializer.data["newpassword"])
        return Response(serializer.data,status=status.HTTP_200_OK)
    
class PriyaUserSerializer(serializers.Serializer):
    class Meta:
        model=priyauser
        fields='__all__'





        