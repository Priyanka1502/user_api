from django.db import models
from .Manager import CustomUserManager
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils import timezone

class priyauser(AbstractBaseUser,PermissionsMixin):
    email=models.EmailField(unique=True)
    name=models.CharField(max_length=255,null=True)
    phone=models.CharField(max_length=267,default=False,null=True)
    is_staff=models.BooleanField(default=False)
    is_superuser=models.BooleanField(default=False)
    is_active=models.BooleanField(default=True)
    date_of_register=models.DateTimeField(default=timezone.now)
    objects=CustomUserManager()


    USERNAME_FIELD='email'
    REQUIRED_FIELDS=['name','phone']
    

    def __str__(self):
        return self.email

# Create your models here.
