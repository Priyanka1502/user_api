from django.urls import path
from .import views

urlpatterns = [
    path('signup', views.Register),
    path('login',views.Login),
    path('singleuser/<str:pk>',views.GetSingleUser),
    path('deleteuser/<str:pk>',views.GetSingleUser),
    path('getallusers/<str:pk>',views.GetAllUsers),
    path('updateuser/<str:pk>',views.UpdateUser),

]
